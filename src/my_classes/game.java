package my_classes;

public class game {
	private data_handler data;

	public game() {
		data = new data_handler();
	}

	public data_handler get_data() {
		return data;
	}

	public boolean login(String username, String phone_Id) {
		return data.check_username_phone_Id(username, phone_Id);
	}
}
