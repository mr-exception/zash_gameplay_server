package my_classes;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class input_channel extends Thread {

	private Socket sck;
	private client cl;
	private game current_game;
	private Scanner in;
	private BufferedInputStream br;
	private wrapper_channel_handler channel_handler;
	private int raw_answer = 0;

	public input_channel(Socket sck, wrapper_channel_handler channel_handler, client c, game cg) {
		this.sck = sck;
		this.current_game = cg;
		this.cl = c;
		this.channel_handler = channel_handler;
		try {
			br = new BufferedInputStream(sck.getInputStream());
			this.in = new Scanner(sck.getInputStream());
		} catch (IOException e) {
			channel_handler.input_channel_not_found(e);
		}
	}

	@Override
	public void run() {
		String message = "";
		while (this.sck.isConnected()) {
			byte[] buffer = new byte[1];
			try {
				br.read(buffer);
				char c = (char) buffer[0];
				if (c == ';') {
					this.channel_handler.recieve(new method(message), this.cl, this.current_game);
					message = "";

				} else if (c > 32 && c < 127) {
					message += c;
				} else {
					this.raw_answer++;
					if (raw_answer > 16) {
						channel_handler.input_channel_closed(message);
						return;
					}
				}
			} catch (IOException e) {
				channel_handler.input_channel_closed(message);
				return;
			}
		}
	}

	public void close() {
		in.close();
	}
}
