package my_classes;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface interface_channel_handler {
	public void recieve(String message);

	public void sent(String message);

	public void input_channel_closed(String last_message);

	public void output_channel_closed();

	public void output_channel_not_found(IOException e);

	public void input_channel_not_found(IOException e);

	// for methods and validator
	public void method_list_not_found(FileNotFoundException e);

	public void error_in_reading_method_list(NumberFormatException e);

	public void wrong_method_Id_got();
}
