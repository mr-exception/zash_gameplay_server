package my_classes;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class output_channel {
	private Socket sck;
	private PrintStream out;
	private wrapper_channel_handler channel_handler;
	private game current_game;

	public output_channel(Socket sck, wrapper_channel_handler ch, game cg) {
		this.sck = sck;
		this.current_game = cg;
		this.channel_handler = ch;
		try {
			this.out = new PrintStream(sck.getOutputStream());
		} catch (IOException e) {
			this.channel_handler.output_channel_not_found(e);
		}
	}

	public void send(String message) {
		if (sck.isConnected()) {
			out.print(message + ";");
			channel_handler.sent(message);
		} else {
			channel_handler.output_channel_closed();
		}
	}
}
