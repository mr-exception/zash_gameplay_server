package my_classes;

import java.io.FileNotFoundException;
import java.io.IOException;

public class wrapper_channel_handler {

	public void recieve(method m, client c, game g) {
		System.out.println("msg recieve: " + m);
		System.out.println("from " + c.get_username());
		if (m.get_method_Id().equals("lg")) {
			// means login
			if (g.login(m.get_params()[0], m.get_params()[1])) {
				c.set_username(m.get_params()[0]);
				c.get_output_channel().send("T");
			} else {
				c.get_output_channel().send("F");
			}
		} else if (m.get_method_Id().equals("by")) {
			// means leave the game
			c.close_connection();
			c.set_username(null);
		}
	}

	public void sent(String message) {
		System.out.println("msg send: " + message);
	}

	public void input_channel_closed(String last_message) {
		System.out.println("input channel closed");
	}

	public void output_channel_closed() {
		System.out.println("output channel closed");
	}

	public void output_channel_not_found(IOException e) {
		System.out.println("output channel not found");
	}

	public void input_channel_not_found(IOException e) {
		System.out.println("input channel not found");
	}

	public void method_list_not_found(FileNotFoundException e) {
		System.out.println("method list file not found");
	}

	public void error_in_reading_method_list(NumberFormatException e) {
		System.out.println("some error while reading method list. please correct valid_methods.txt");
	}

	public void wrong_method_Id_got() {
		System.out.println("wrong method got from input channel");
	}

	public void error_in_connection(int port) {
		System.out.println("can't bind for a connection in " + port);
	}

	public void cant_close_socket(String username) {
		System.out.println("can't close the meeting from " + username);
	}

	public void closed_socket(String username) {
		System.out.println("closed the meeting from " + username);
	}
}
