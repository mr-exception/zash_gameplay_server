package my_classes;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * this class hold the information of a socket channel between server and just one client.
 * exactly complies all the data got from connection channel to runnable methods or convert 
 * runnable methods to data sent in channels
 */
public class client {
	/*
	 * connection models: input and output streams connection layers
	 */
	private Socket sck;
	private input_channel input;
	private output_channel output;
	private wrapper_channel_handler channel_handler;
	private game current_game;
	/*
	 * user informations
	 */
	private String username = null;

	public client(Socket sck, wrapper_channel_handler ch, game cg) throws IOException {
		// when this object is going to create we have the connection to client
		this.channel_handler = ch;
		this.current_game = cg;
		this.sck = sck;
		this.input = new input_channel(sck, this.channel_handler, this, this.current_game);
		this.input.start();
		this.output = new output_channel(sck, this.channel_handler, this.current_game);
	}

	/*
	 * test case: tested input channel worked fine!
	 */
	public static void main(String[] args) {
		wrapper_channel_handler ch = new wrapper_channel_handler();
		game main_game = new game();
		method.set_channel_handler(ch);
		try {
			ServerSocket server = new ServerSocket(2525);
			System.out.println("connection waiting on " + 2525);
			Socket sck = server.accept();
			System.out
					.println("connected in " + sck.getLocalAddress() + " (size = " + sck.getReceiveBufferSize() + ")");
			client c = new client(sck, ch, main_game);
			while (true)
				;
		} catch (IOException e) {
			ch.error_in_connection(2525);
		}

	}

	public String get_username() {
		return username;
	}

	public void set_username(String username) {
		this.username = username;
	}

	public boolean logged_in() {
		return username != null;
	}

	public output_channel get_output_channel() {
		return this.output;
	}

	public input_channel get_input_channel() {
		return this.input;
	}

	public boolean close_connection() {
		try {
			this.sck.close();
			this.channel_handler.closed_socket(username);
			return true;
		} catch (IOException e) {
			this.channel_handler.cant_close_socket(username);
			return false;
		}
	}
}
