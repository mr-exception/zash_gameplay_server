package my_classes;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class method {
	private String method_Id;
	private String[] params;
	public static final String METHOD_SEPERATOR = "@";
	public static final String PARAM_SEPERATOR = "/";
	private static HashMap<String, Integer> dictionary = null;
	private static wrapper_channel_handler channel_handler;

	public method(String arg) {
		load_dictionary();
		String[] data = arg.split(METHOD_SEPERATOR);
		this.method_Id = data[0];
		if (arg.contains(METHOD_SEPERATOR)) {
			this.params = data[1].split(PARAM_SEPERATOR);
			// check if arg method is valid
			if (!dictionary.containsKey(method_Id)) {
				wrong_method();
				return;
			}
			if (!(dictionary.get(method_Id) == params.length)) {
				wrong_method();
				return;
			}
		} else {
			if (!(dictionary.containsKey(this.method_Id))) {
				System.out.println("flag");
				wrong_method();
				return;
			}
			if (!(dictionary.get(this.method_Id) == 0)) {
				wrong_method();
				return;
			}

		}
	}

	private void wrong_method() {
		channel_handler.wrong_method_Id_got();
		method_Id = "unknown";

	}

	public String toString() {
		String result = "[" + method_Id + ":";
		if (is_single_method()) {
			result += "]";
		} else {
			for (int i = 0; i < params.length - 1; i++)
				result += params[i] + ",";
			result += params[params.length - 1] + "]";
		}
		return result;
	}

	private static void load_dictionary() {
		if (dictionary == null) {
			dictionary = new HashMap<>();
			Scanner in;
			try {
				in = new Scanner(new java.io.File("valid_methods.txt"));
				while (in.hasNextLine()) {
					String line = in.nextLine();
					if (line.startsWith("#"))
						continue;
					String[] parts = line.split(":");
					dictionary.put(parts[0], Integer.valueOf(parts[1]));
				}
			} catch (FileNotFoundException e) {
				channel_handler.method_list_not_found(e);
			} catch (NumberFormatException e) {
				channel_handler.error_in_reading_method_list(e);
			}

		}
	}

	public String get_method_Id() {
		return this.method_Id;
	}

	public String[] get_params() {
		return this.params;
	}

	public boolean is_single_method() {
		return params == null;
	}

	public static void set_channel_handler(wrapper_channel_handler ich) {
		channel_handler = ich;
	}

	/*
	 * test case: tested, worked fine
	 *
	 * public static void main(String[] args) { method.set_channel_handler(new
	 * wrapper_channel_handler()); method m = new method("m:1,2,3"); }
	 */
}
